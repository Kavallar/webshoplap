﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webshop_Games.Data;
using Webshop_Games.Models;

namespace Webshop_Games.Logic
{
    public class OrdersManager
    {
        public static Orders_VM GetOrders(CartManager cart)
        {
            using (var orders = new WebshopEntities())
            {
                Orders_VM orders_ = new Orders_VM();
                orders_.OrderedProducts = new List<OrderedProducts>();

                var user = orders.User.Where(x => x.id == cart.Id).FirstOrDefault();

                orders_.Tilte = user.Title;
                orders_.FirstName = user.FirstName;
                orders_.LastName = user.LastName;
                orders_.Street = user.Street;
                orders_.ZipCode = user.ZipCode;
                orders_.City = user.City;

                var billAdr = new Data.BillingAdress()
                {
                    Firstname = user.FirstName,
                    Lastname = user.LastName,
                    Street = user.Title,
                    Zipcode = user.ZipCode
                };
                
                foreach (var item in cart.Cart)
                {
                    OrderedProducts newOrderedProd = new OrderedProducts();

                    var dbProduct = orders.Product.Where(x => x.id == item.Key).FirstOrDefault();

                    newOrderedProd.ProductName = dbProduct.Name;
                    newOrderedProd.CompanyName = orders.Company.Where(x => x.id == dbProduct.Company_id).Select(x => x.Name).FirstOrDefault();
                    newOrderedProd.Quantity = item.Value;
                    newOrderedProd.LinePrice = orders.Product.Where(x => x.id == item.Key).Select( x=> x.Price).FirstOrDefault() * item.Value;
                    orders_.OrderedProducts.Add(newOrderedProd);
                }

                //totalPrice berechnen einfügen
                orders_.TotalPrice = orders_.OrderedProducts.Sum(x => x.LinePrice);
                
                return orders_;
            }
        }

        public static int SaveOrder(Dictionary<int, int> cart, int userId, Data.BillingAdress billingAdress)
        {
            try
            {
                using (var db = new WebshopEntities())
                {
                    var user = db.User.Where(x => x.id == userId).FirstOrDefault();

                    if (String.IsNullOrWhiteSpace(billingAdress.Lastname))
                    {
                        billingAdress.Lastname = user.LastName;
                    }
                    if (String.IsNullOrWhiteSpace(billingAdress.Firstname))
                    {
                        billingAdress.Firstname = user.FirstName;
                    }
                    if (String.IsNullOrWhiteSpace(billingAdress.Street))
                    {
                        billingAdress.Street = user.Street;
                    }
                    if (String.IsNullOrWhiteSpace(billingAdress.Zipcode))
                    {
                        billingAdress.Zipcode = user.ZipCode;
                    }
                    if (String.IsNullOrWhiteSpace(billingAdress.City))
                    {
                        billingAdress.City = user.City;
                    }

                    db.BillingAdress.Add(billingAdress);
                    db.SaveChanges();


                    var order = new Order();
                    order.Billing_Adress_id = billingAdress.id;
                    order.User_id = userId;
                    order.DateOrdered = DateTime.Now;
                    
                    // Wenn Zahlmethode inplemeniert wird, DateTime.Now; zu NULL ändern und für Zahldatum eine Zahlungsmethode erstellen
                    order.DatePaid = DateTime.Now;

                    db.Order.Add(order);
                    db.SaveChanges();

                    foreach (var item in cart)
                    {
                        var orderLine = new Data.OrderLine();
                        var product = db.Product.Where(x => x.id == item.Key).FirstOrDefault();

                        orderLine.Order_id = order.id;
                        orderLine.Product_id = item.Key;
                        orderLine.Quantity = item.Value;
                        orderLine.UnitPrice = product.Price;
                        orderLine.LinePrice = product.Price * orderLine.Quantity.Value;

                        db.OrderLine.Add(orderLine);
                    }
                        db.SaveChanges();

                    order.PriceTotal = db.OrderLine.Where(x => x.Order_id == order.id).Sum(x => x.LinePrice);
                    order.PriceNet = (order.PriceTotal / 120) * 100;
 
                    db.SaveChanges();
                    return order.id;
                } 
            }
                catch (Exception)
                {
                    throw;
                }
        }

        public static Customer_VM GetCustomerOrders(int userId)
        {
            using (var db = new WebshopEntities())
            {
                var dbCustomerOrders = db.Order.Where(x => x.User_id == userId).ToList();

                var orders = new Customer_VM();

                orders.UserId = userId;
                orders.CustomerOrders = new List<CustomerOrders>();

                foreach (var item in dbCustomerOrders)
                {
                    var order = new CustomerOrders();
                    order.OrderId = item.id;
                    order.DateOrdered = item.DateOrdered.Value;
                    order.PriceTotal = item.PriceTotal;
                    order.DatePaid = item.DatePaid.Value;

                    var orderLines = db.OrderLine.Where(x => x.Order_id == item.id).ToList();
                    order.Orderline = new List<Models.OrderLine>();

                    foreach (var line in orderLines)
                    {
                        var lines = new Models.OrderLine();

                        lines.Id = line.id;
                        lines.Quantity = line.Quantity.Value;
                        lines.UnitPrice = line.UnitPrice;
                        lines.ProductId = line.Product_id.Value;
                        
                        var product = db.Product.Where(x => x.id == line.Product_id).FirstOrDefault();
                        lines.ProductName = product.Name;
                        lines.CompanyName = db.Company.Where(x => x.id == product.Company_id).Select(x => x.Name).FirstOrDefault();
                        lines.ImagePath = product.ImagePath;
                        lines.LinePrice = line.LinePrice;

                        order.Orderline.Add(lines);
                    }
                    orders.CustomerOrders.Add(order);

                }
                return orders;
            }
        }
    }
}