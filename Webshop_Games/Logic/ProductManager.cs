﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webshop_Games.Data;
using Webshop_Games.Models;

namespace Webshop_Games.Logic
{
    public class ProductManager
    {
        public static List<Product_VM> GetProduct()
        {
            using (var db = new WebshopEntities())
            {
                List<Product_VM> products = new List<Product_VM>();

                foreach (var item in db.Product)
                {
                    var newProduct = new Product_VM();

                    newProduct.Id = item.id;
                    newProduct.Name = item.Name;
                    newProduct.Price = item.Price;
                    newProduct.ImagePath = item.ImagePath;
                    newProduct.Company_id = item.Company_id;
                    newProduct.CompanyName = db.Company.Where(x => x.id == item.Company_id).Select(x => x.Name).FirstOrDefault();
                    newProduct.Category_id = item.Category_id; 
                    newProduct.CategoryName = db.Category.Where(x => x.id == item.Category_id).Select(x => x.Name).FirstOrDefault();
                    newProduct.Description = item.Description;

                    products.Add(newProduct);
                }
                return products;
            }   
        }
    }
}