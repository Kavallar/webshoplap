﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.IO;

namespace Webshop_Games.Logic
{
    public class SendEmail
    {

        public static void Send_Email(int orderId, Stream attachement, string email)
        {
            
            

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("ITN187529@qualifizierung.at"); //Absender 
            mail.To.Add(email); //Empfänger
            mail.Subject = "Ihre Bestellung";
            mail.Body = "<h1>Wir danken für Ihre Bestellung.</h1>"+
                        $"<p>Ihre Bestellnummer lautet {orderId}</p>";
            mail.IsBodyHtml = true; //Nur wenn Body HTML Quellcode ist  

            SmtpClient client = new SmtpClient("srv009.edu.local", 587); //SMTP Server von Hotmail und Outlook.

            mail.Attachments.Add(new Attachment(attachement, "Rechnung.pdf")); 

            try
            {
                client.Credentials = new System.Net.NetworkCredential("ITN187529@edu.local", "Teufelchen1bb");
                client.EnableSsl = false; //Die meisten Anbieter verlangen eine SSL-Verschlüsselung 

                client.Send(mail); //Senden 

                Console.WriteLine("E-Mail wurde versendet");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fehler beim Senden der E-Mail\n\n{0}", ex.Message);
            }
        }
    }

}
