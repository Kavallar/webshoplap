﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Webshop_Games.Logic
{
    public class Hasher
    {
        public static string GetRandomBytes(int size)
        {
            var rndBytes = new byte[size];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(rndBytes);

                var hashstring = "";

                foreach (var item in rndBytes)
                {
                    hashstring += item.ToString("X2");
                }
                return hashstring;
            }
        }

        public static string HashAndSaltString256(string pwd, string salt)
        {
            using (var hasher = new SHA256Managed())
            {
                string saltedPw = pwd + salt;
                //Bytes aus String holen, benötigt System.Text
                byte[] saltedPwBytes = Encoding.ASCII.GetBytes(saltedPw);

                //Password+Salt hashen
                var hashedAndSaltedPw = hasher.ComputeHash(saltedPwBytes);

                var saltstring = "";

                foreach (var item in hashedAndSaltedPw)
                {
                    saltstring += item.ToString("X2");
                }

                //Hashwert zurückgeben
                return saltstring;
            }
        }
    }
}