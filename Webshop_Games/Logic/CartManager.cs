﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webshop_Games.Logic
{
    public class CartManager
    {
        private readonly Dictionary<int, int> _cart;

        public Dictionary<int, int> Cart
        {
            get
            {
                return _cart ?? throw new Exception("_chart ist NULL");
            }
        }

        public int Id { get; set; }
        public int UserId { get;  set; }
        public decimal PriceTotal { get; set; }
        public decimal PriceNet { get; set; }

        public int Empty()
        {
            if (_cart.Count() == 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        } 

        public CartManager(Dictionary<int, int> sessionCart)
        {
            if (sessionCart == null)
            {
                _cart = new Dictionary<int, int>();
            }
            else
            {
                _cart = sessionCart;
            }
        }

        public void Add(int id, int qty)
        {
            if (qty <= 0)
            {
                return;
            }

            int oldQty = 0;

            if (_cart.TryGetValue(id, out oldQty)) 
            {
                _cart[id] = oldQty + qty;
            }
            else
            {
                _cart.Add(id, qty);
            }
        }

        public void Remove(int id)
        {
            _cart.Remove(id);
        }

        public void Remove(int id, int qty)
        {
            if (qty <= 0)
            {
                return;
            }

            int oldQty = 0;

            if (_cart.TryGetValue(id, out oldQty)) 
            {
                if (oldQty > 1) 
                {
                    _cart[id] = oldQty - qty; 
                }
                else 
                {
                    _cart.Remove(id); 
                }
            }        
        }

        public void ClearCart()
        {
            _cart.Clear();
        }
    }
}