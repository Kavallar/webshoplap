﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Webshop_Games
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_AuthenticateRequesest(Object sender, EventArgs e)
        {
            HttpCookie cookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (Context.Request.Cookies[FormsAuthentication.FormsCookieName] == null)
            {
                return;
            }

            var ticket = FormsAuthentication.Decrypt(cookie.Value);

            var identity = new GenericIdentity(ticket.Name);

            var principal = new GenericPrincipal(identity, null);

            Context.User = principal;
        }

    }
}
