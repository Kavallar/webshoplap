﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webshop_Games.Models;
using Webshop_Games.Data;
using Webshop_Games.Logic;
using System.Web.Security;

namespace Webshop_Games.Controllers
{
    public class LoginController : Controller
    {
        // GET: Registry
        public ActionResult Registry()
        {
            var user = new Registry_VM();
            return View(user);
        }

        // POST: Registry
        [HttpPost]
        public ActionResult Registry(Registry_VM user)
        {
           
            using (var db = new WebshopEntities())
            {
                if (db.User.Any(u => u.email == user.Email))
                {
                    return RedirectToAction("Registry");
                }
                else
                {
                    var newUser = new User();
                    newUser.Title = user.Title;
                    newUser.LastName = user.Lastname;
                    newUser.FirstName = user.Firstname;
                    newUser.email = user.Email;
                    newUser.Street = user.Street;
                    newUser.ZipCode = user.ZipCode;
                    newUser.City = user.City;

                    var password = user.Password;

                    //Salt erzeugen
                    newUser.Salt = Hasher.GetRandomBytes(256 / 8); //entspricht 32 Bytes

                    //Password Hashen
                    newUser.PasswordHash = Hasher.HashAndSaltString256(password, newUser.Salt);
               
                    db.User.Add(newUser);
                    db.SaveChanges();
                    return RedirectToAction("Login");                  
                }          
            }           
        }

        // GET: Login
        public ActionResult Login()

        {
            var user = new Login_VM();
            return View(user);
        }

        // POST Login
        [HttpPost]
        public ActionResult Login(Login_VM user)
        {
            using (var db = new WebshopEntities())
            {
                var loginUser = db.User.Where(l => l.email == user.Email).FirstOrDefault();

                string hasher = Hasher.HashAndSaltString256(user.Password, loginUser.Salt);

                if (loginUser.PasswordHash == hasher && loginUser.email == user.Email)
                {
                    //Create an user ticket
                    var ticket = new FormsAuthenticationTicket(
                        1,
                        user.Email,
                        DateTime.Now,
                        DateTime.Now.AddMinutes(30),
                        false,
                        ""
                        );

                    //Encrypt ticket
                    var encryptedTicket = FormsAuthentication.Encrypt(ticket);

                    //create a cookie
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                    //Send cookie to client
                    HttpContext.Response.Cookies.Add(cookie);          
                }

                return RedirectToAction("Index", "Product");        
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
    }
}
