﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webshop_Games.Data;
using Webshop_Games.Models;
using Webshop_Games.Logic;

namespace Webshop_Games.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        [Authorize]
        public ActionResult OrdersList()
        {
            using (var db = new WebshopEntities())
            {
                var user = db.User.Where(x => x.email == User.Identity.Name).Select(x => x.id).FirstOrDefault();
                var proList = OrdersManager.GetCustomerOrders(user);

                return View(proList);
            }
        }
    }
}