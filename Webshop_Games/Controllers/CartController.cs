﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webshop_Games.Data;
using Webshop_Games.Models;
using Webshop_Games.Logic;

namespace Webshop_Games.Controllers
{
    public class CartController : Controller
    {
        [Authorize]
        public ActionResult Cart()
        {
            using (var db = new WebshopEntities())
            {
                var newCart = Session.GetCart();
                decimal totalPrice = 0;

                var dictonaryOfItems = newCart.Cart;

                var listOfProducts = new List<Product>();


                foreach (var item in dictonaryOfItems)
                {
                    var newPro = new Product();

                    newPro = db.Product.Where(x => x.id == item.Key).FirstOrDefault();

                    listOfProducts.Add(newPro);
                }

                //newCart.Id = db.Order.Where(x => x.id == newCart.Id).Select(x => x.id).FirstOrDefault();
                //newCart.UserId = db.User.Where(x => x.id == newCart.UserId).Select(x => x.id).FirstOrDefault();
                //newCart.UserId = db.User.Where(x => x.email == User.Identity.Name).Select(x => x.id).FirstOrDefault();

                List<CartProducts> cartProducts = new List<CartProducts>();

                foreach (var item in listOfProducts)
                {
                    var newCartProduct = new CartProducts();

                    newCartProduct.ProductId = db.Product.Where(x => x.id == item.id).Select(x => x.id).FirstOrDefault();
                    newCartProduct.ImagePath = db.Product.Where(x => x.ImagePath == item.ImagePath).Select(x => x.ImagePath).FirstOrDefault();
                    newCartProduct.Name = db.Product.Where(x => x.Name == item.Name).Select(x => x.Name).FirstOrDefault();
                    // get Companyname by FK in Products
                    newCartProduct.CompanyId = item.Company_id;
                    newCartProduct.CompanyName = db.Company.Where(x => x.id == item.Company_id).Select(x => x.Name).FirstOrDefault();

                    newCartProduct.Price = db.Product.Where(x => x.Price == item.Price).Select(x => x.Price).FirstOrDefault();

                    //newCartProduct.OrderLineId = db.OrderLine.Where(x => x.Order_id == item.id).Select(x => x.Order_id).FirstOrDefault();
                    //newCartProduct.Quantity = db.OrderLine.Where(x => x.Order_id == item.id).Select(x => x.Quantity).FirstOrDefault();

                    int qty = dictonaryOfItems.Where(x => x.Key == item.id).Select(x => x.Value).FirstOrDefault();
                    newCartProduct.Quantity = qty;
                    newCartProduct.LinePrice = newCartProduct.Price * qty;
                    totalPrice += newCartProduct.LinePrice;

                    cartProducts.Add(newCartProduct);
                }

                var cart = new Cart_VM();
                cart.Product = cartProducts;
                cart.PriceTotal = totalPrice;
                cart.PriceNet = (totalPrice / 120) * 100;

                return View(cart);
            }
            
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddOneProductToChart(int proId)
          {
            var newQty = Session.GetCart();
            if (!newQty.Cart.Keys.Contains(proId))
            {
                newQty.Add(proId, 1);
                Session.SaveCart(newQty);
            }
            else
            {
                ViewBag.Error = "Produkt befindet sich schon im wk";
            }

            return RedirectToAction("Index", "Product");
        }

        [HttpPost]
        [Authorize]
        public ActionResult Add(int proId, int qty)
        {
            var newQty = Session.GetCart();
            newQty.Add(proId, qty);
            Session.SaveCart(newQty);
            return RedirectToAction("Index", "Product");
        }

        [HttpPost]
        [Authorize]
        public ActionResult Remove(int proId, int qty)
        {
            var newQty = Session.GetCart();
            newQty.Remove(proId, qty);
            Session.SaveCart(newQty);
            return RedirectToAction("Index", "Product");
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChangeQty(int proId, int qty, bool? info)
        
        {
            if (info == null )
            {
                var newQty = Session.GetCart();
                newQty.Remove(proId);
                Session.SaveCart(newQty);
            }
            else if (info == true)
            {
                Add(proId, qty);
            }
            else
            {
                Remove(proId, qty);
            }
            return RedirectToAction("Details", "Product", new { id = proId });
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChangeQty2(int proId, int qty, bool? info)

        {
            if (info == null)
            {
                var newQty = Session.GetCart();
                newQty.Remove(proId);
                Session.SaveCart(newQty);
            }
            else if (info == true)
            {
                Add(proId, qty);
            }
            else
            {
                Remove(proId, qty);
            }
            return RedirectToAction("Cart", "Cart", new { id = proId });
        }
    }

    public static class SessionExtensions
    {
        public static CartManager GetCart(this HttpSessionStateBase session)
        {
            return new CartManager((Dictionary<int, int>)session["Cart"]);
        }
        public static void SaveCart(this HttpSessionStateBase session, CartManager mgr)
        {
            session["Cart"] = mgr.Cart;
        }
    }
}