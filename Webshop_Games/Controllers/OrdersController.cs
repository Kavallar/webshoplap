﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webshop_Games.Data;
using Webshop_Games.Models;
using Webshop_Games.Logic;
using Rotativa;
using Rotativa.MVC;
using System.IO;

namespace Webshop_Games.Controllers
{
    public class OrdersController : Controller
    {
        // GET: Ordered
        public ActionResult GetOrders()
        {
            CartManager cart = Session.GetCart();

            // Überprüfung ob Waren im Warenkorb sind
            if (cart.Empty() == 1)
            {
                return RedirectToAction("CartIsEmpty");
            }
            else
            {

            using (var db = new WebshopEntities())
            {
                int id = db.User.Where(x => x.email == User.Identity.Name).Select(x => x.id).FirstOrDefault();
                cart.Id = id;
            }

            Orders_VM orders = OrdersManager.GetOrders(cart);

            return View(orders);
            }
        }

        // Post: SaveOrder
        [HttpPost]
        public ActionResult SaveOrder(Data.BillingAdress billing)
        {
            CartManager cart = Session.GetCart();

            using (var db = new WebshopEntities())
            {
                int id = db.User.Where(x => x.email == User.Identity.Name).Select(x => x.id).FirstOrDefault();
                int ordId = OrdersManager.SaveOrder(cart.Cart, id, billing);
                var user = db.User.Where(x => x.id == id).FirstOrDefault();
                var order = db.Order.Where(x => x.id == ordId).FirstOrDefault();

                ViewBag.OrderId = ordId;

                var orders_VM = new Orders_VM();
                orders_VM.UserId = id;
                orders_VM.OrderId = ordId;
                orders_VM.Tilte = user.Title;
                orders_VM.FirstName = user.FirstName;
                orders_VM.LastName = user.LastName;
                orders_VM.TotalPrice = order.PriceTotal;
                orders_VM.NetPrice = order.PriceNet;
                orders_VM.Street = user.Street;
                orders_VM.ZipCode = user.ZipCode;
                orders_VM.City = user.City;
                orders_VM.DateOrdered = order.DateOrdered;

                var dbBillingAdress = db.BillingAdress.Where(x => x.id == order.Billing_Adress_id).FirstOrDefault();
                var billingAdress = new Models.BillingAdress()
                {
                    id = dbBillingAdress.id,
                    Title = dbBillingAdress.Title,
                    Firstname = dbBillingAdress.Firstname,
                    Lastname = dbBillingAdress.Lastname,
                    Street = dbBillingAdress.Street,
                    ZipCode = dbBillingAdress.Zipcode,
                    City = dbBillingAdress.City
                };

                var orderline = db.OrderLine.Where(x => x.Order_id == ordId).ToList();
                orders_VM.OrderedProducts = new List<OrderedProducts>();

                foreach (var item in orderline)
                {
                    var orderedProduct = new OrderedProducts();
                    var product = db.Product.Where(x => x.id == item.Product_id).FirstOrDefault();
                    var orderLine = new OrderedProducts();
                    var comp = db.Company.Where(x => x.id == product.Company_id).FirstOrDefault();
                    orderedProduct.ProductName = product.Name;
                    orderedProduct.CompanyName = comp.Name;
                    orderedProduct.Quantity = item.Quantity ?? 0;
                    orderedProduct.LinePrice = item.LinePrice;

                    orders_VM.OrderedProducts.Add(orderedProduct);

                }
                var pdf = new ViewAsPdf("PDF", orders_VM);
                var byteArr = pdf.BuildPdf(ControllerContext);
                Stream pdfArray = new MemoryStream(byteArr);

                string email = user.email;

                SendEmail.Send_Email(ordId, pdfArray, email);
                
                cart.ClearCart();

                return View("SaveOrder");
            }
        }

        public ActionResult CartIsEmpty()
        {
            string cartEmpty = "Ihr Warenkorb ist noch leer.";
            ViewBag.message = cartEmpty;

            return View();
        }
        
    }
}