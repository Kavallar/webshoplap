﻿using Rotativa.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webshop_Games.Data;
using Webshop_Games.Models;

namespace Webshop_Games.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Impressum()
        {
            return View();
        }

        public ActionResult Datenschutz()
        {
            return View();
        }

        public ActionResult AGB()
        {
            return View();
        }

        public ActionResult PDF()
        {
            var id = 2;
            var ordId = 1053;
            using (var db = new Webshop_Games.Data.WebshopEntities())
            {
                var user = db.User.Where(x => x.id == id).FirstOrDefault();
                var order = db.Order.Where(x => x.id == ordId).FirstOrDefault();
                var orders_VM = new Orders_VM();
                orders_VM.UserId = id;
                orders_VM.OrderId = ordId;
                orders_VM.Tilte = user.Title;
                orders_VM.FirstName = user.FirstName;
                orders_VM.LastName = user.LastName;
                orders_VM.TotalPrice = order.PriceTotal;
                orders_VM.NetPrice = order.PriceNet;
                orders_VM.Street = user.Street;
                orders_VM.ZipCode = user.ZipCode;
                orders_VM.City = user.City;
                orders_VM.DateOrdered = order.DateOrdered;

                var dbBillingAdress = db.BillingAdress.Where(x => x.id == order.Billing_Adress_id).FirstOrDefault();
                var billingAdress = new Models.BillingAdress()
                {
                    id = dbBillingAdress.id,
                    Title = dbBillingAdress.Title,
                    Firstname = dbBillingAdress.Firstname,
                    Lastname = dbBillingAdress.Lastname,
                    Street = dbBillingAdress.Street,
                    ZipCode = dbBillingAdress.Zipcode,
                    City = dbBillingAdress.City
                };

                var orderline = db.OrderLine.Where(x => x.Order_id == ordId).ToList();
                orders_VM.OrderedProducts = new List<OrderedProducts>();

                foreach (var item in orderline)
                {
                    var orderedProduct = new OrderedProducts();
                    var product = db.Product.Where(x => x.id == item.Product_id).FirstOrDefault();
                    var orderLine = new OrderedProducts();
                    var comp = db.Company.Where(x => x.id == product.Company_id).FirstOrDefault();
                    orderedProduct.ProductName = product.Name;
                    orderedProduct.CompanyName = comp.Name;
                    orderedProduct.Quantity = item.Quantity ?? 0;
                    orderedProduct.LinePrice = item.LinePrice;

                    orders_VM.OrderedProducts.Add(orderedProduct);
                }
                    return new ViewAsPdf(orders_VM);

            }

        }
    }
}