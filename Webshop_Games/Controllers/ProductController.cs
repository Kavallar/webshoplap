﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webshop_Games.Data;
using Webshop_Games.Models;
using Webshop_Games.Logic;

namespace Webshop_Games.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            var proList = ProductManager.GetProduct();

            var cat = new Dictionary<int, string>();

            using (var db = new WebshopEntities())
            {
                foreach (var item in db.Category)
                {
                    cat.Add(item.id, item.Name);
                }
            }

            TempData["Categories"] = cat;

            return View(proList);

            //return View(new List<Models.Product>());
           
        }

        [HttpGet]
        public ActionResult Search(int catId, string search)
        {
            List<Product_VM> products = new List<Product_VM>();
            if(catId != 1)
            {
                products = ProductManager.GetProduct().Where(x => x.Category_id == catId).ToList();
            }
            else
            {
                products = ProductManager.GetProduct();
            }
            if(search != null)
            {
                products = products.Where(x =>  x.Name.ToLower().Contains(search.ToLower()) || 
                                                x.Description.ToLower().Contains(search.ToLower()) ||
                                                x.CompanyName.ToLower().Contains(search.ToLower())).ToList();
            }

            var cat = new Dictionary<int, string>();

            using (var db = new WebshopEntities())
            {
                foreach (var item in db.Category)
                {
                    cat.Add(item.id, item.Name);
                }
            }

            TempData["Categories"] = cat;

            return View("Index", products);
        }
   
        public ActionResult Details(int id)
        {
            using (var db = new WebshopEntities())
            {
                var newCart = Session.GetCart();

                var details = db.Product.Where(x => x.id == id).SingleOrDefault();

                var newDetails = new Details_VM();

                newDetails.Id = details.id;
                newDetails.Name = details.Name;
                newDetails.Description = details.Description;
                newDetails.Price = details.Price;
                newDetails.ImagePath = details.ImagePath;
                newDetails.Company_id = db.Company.Where(x => x.id == details.Company_id).Select(x => x.id).FirstOrDefault(); ;
                newDetails.CompanyName = db.Company.Where(x => x.id == details.Company_id).Select(x => x.Name).FirstOrDefault();
                newDetails.Category_id = db.Category.Where(x => x.id == details.Category_id).Select(x => x.id).FirstOrDefault();
                newDetails.CategoryName = db.Category.Where(x => x.id == details.Category_id).Select(x => x.Name).FirstOrDefault();
                newDetails.Quantity = newCart.Cart.Where(x => x.Key == id).Select(x => x.Value).FirstOrDefault();

                return View(newDetails);
            }
        }
    }
}
