//------------------------------------------------------------------------------
// <auto-generated>
//     Der Code wurde von einer Vorlage generiert.
//
//     Manuelle Änderungen an dieser Datei führen möglicherweise zu unerwartetem Verhalten der Anwendung.
//     Manuelle Änderungen an dieser Datei werden überschrieben, wenn der Code neu generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Webshop_Games.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            this.OrderLine = new HashSet<OrderLine>();
        }
    
        public int id { get; set; }
        public Nullable<System.DateTime> DateOrdered { get; set; }
        public decimal PriceNet { get; set; }
        public decimal PriceTotal { get; set; }
        public Nullable<System.DateTime> DatePaid { get; set; }
        public int User_id { get; set; }
        public Nullable<int> Billing_Adress_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderLine> OrderLine { get; set; }
        public virtual User User { get; set; }
        public virtual BillingAdress BillingAdress { get; set; }
    }
}
