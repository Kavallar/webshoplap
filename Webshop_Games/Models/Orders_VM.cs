﻿using System;
using System.Collections.Generic;

namespace Webshop_Games.Models
{
    public class Orders_VM
    {
        public int UserId { get; set; }
        public int OrderId { get; set; }
        public string Tilte { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<OrderedProducts> OrderedProducts { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal NetPrice { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public DateTime? DateOrdered { get; set; }
        public BillingAdress BillAdress{ get; set; }
    }

    public class OrderedProducts
    {
        public string ProductName { get; set; }
        public string CompanyName { get; set; }
        public int Quantity { get; set; }
        public decimal LinePrice { get; set; }
    }
}