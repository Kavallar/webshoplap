﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webshop_Games.Models
{
    public class BillingAdress
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
    }
}