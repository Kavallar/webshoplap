﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webshop_Games.Models
{
    public class Customer_VM
    {
        public int UserId { get; set; }
        //public string Tilte { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        public List<CustomerOrders> CustomerOrders { get; set; }
    }

    public class CustomerOrders
    {
        public int OrderId { get; set; }
        public DateTime DateOrdered { get; set; }
        public decimal PriceTotal { get; set; }
        public DateTime DatePaid { get; set; }

        public List<OrderLine> Orderline { get; set; }
    }

    public class OrderLine
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string CompanyName { get; set; }
        public string ImagePath { get; set; }
        public decimal LinePrice { get; set; }
    }
    
}