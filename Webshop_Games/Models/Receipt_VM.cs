﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webshop_Games.Models
{
    public class ReceiptVmProduct
    {
        public string Name { get; set; }
        public int Ouantity { get; set; }
        public decimal LinePrice { get; set; }
    }

    public class Receipt_VM
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Netprice { get; set; }

        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }

        public List<ReceiptVmProduct> Products { get; set; }

        public Receipt_VM()
        {
            Products = new List<ReceiptVmProduct>();
        }
    }
}