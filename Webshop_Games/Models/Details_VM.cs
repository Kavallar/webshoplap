﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webshop_Games.Models
{
    public class Details_VM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string ImagePath { get; set; }
        public int Company_id { get; set; }
        public string CompanyName { get; set; }
        public int Category_id { get; set; }
        public string CategoryName { get; set; }
        public int Quantity { get; set; }
    }
}