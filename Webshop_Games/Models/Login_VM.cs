﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webshop_Games.Models
{
    public class Login_VM
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
    }
}