﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace Webshop_Games.Models
{
    public class Registry_VM
    {
        [Required,DisplayName("Anrede")]
        public string Title { get; set; }
        [Required, DisplayName("Nachname")]
        public string Lastname { get; set; }
        [Required, DisplayName("Vorname")]
        public string Firstname { get; set; }
        [Required, EmailAddress, DisplayName("E-mail")]
        public string Email { get; set; }
        [Required, PasswordPropertyText, DisplayName("Passwort"), MinLength(8)]
        [RegularExpression("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^(.{8,15})$", ErrorMessage = "Passwort muß min. 8 Stellen lang sein und min einen Klein-, einen Großbuchstaben und eine Zahl enthalten.")]
        public string Password { get; set; }
        [Required,PasswordPropertyText, Compare("Password"), DisplayName("Wiederholung")]
        public string PasswordConfirm { get; set; }
        [Required, DisplayName("Straße")]
        public string Street { get; set; }
        [Required, DisplayName("PLZ")]
        public string ZipCode { get; set; }
        [Required, DisplayName("Ort")]
        public string City { get; set; }
    }
}