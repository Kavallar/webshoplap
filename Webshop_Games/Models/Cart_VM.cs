﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webshop_Games.Models
{
    public class Cart_VM
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public List<CartProducts> Product { get; set; }
        public decimal PriceNet { get; set; }
        public decimal PriceTotal { get; set; }       
    }

    public class CartProducts
    {
        public int ProductId { get; set; }
        //Productname
        public string Name { get; set; }
        //Bild
        public string ImagePath { get; set; }        
        //Herrsteller
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        //Preis pro Einheit
        public decimal Price { get; set; }
        //Menge
        public int? OrderLineId { get; set; }
        public int? Quantity { get; set; }
        //Zeilenpreis
        public decimal LinePrice { get; set; }
    }
}